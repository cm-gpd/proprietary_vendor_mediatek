# Copyright (C) 2019 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE               := mtk_omx_core.cfg
LOCAL_SRC_FILES            := etc/mtk_omx_core.cfg
LOCAL_MODULE_CLASS         := ETC
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libMtkOmxCore
LOCAL_SRC_FILES            := lib/libMtkOmxCore.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libMtkOmxMp3Dec
LOCAL_SRC_FILES            := lib/libMtkOmxMp3Dec.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libMtkOmxVdecEx
LOCAL_SRC_FILES            := lib/libMtkOmxVdecEx.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libMtkOmxVenc
LOCAL_SRC_FILES            := lib/libMtkOmxVenc.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libstagefrighthw
LOCAL_SRC_FILES            := lib/libstagefrighthw.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libClearMotionFW
LOCAL_SRC_FILES            := lib/libClearMotionFW.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libmhalImageCodec
LOCAL_SRC_FILES            := lib/libmhalImageCodec.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libvcodecdrv
LOCAL_SRC_FILES            := lib/libvcodecdrv.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libvcodec_utility
LOCAL_SRC_FILES            := lib/libvcodec_utility.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libJpgDecPipe
LOCAL_SRC_FILES            := lib/libJpgDecPipe.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libJpgEncPipe
LOCAL_SRC_FILES            := lib/libJpgEncPipe.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libmp4enc_sa.ca7
LOCAL_SRC_FILES            := lib/libmp4enc_sa.ca7.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := librrc
LOCAL_SRC_FILES            := lib/librrc.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libvc1dec_sa.ca7
LOCAL_SRC_FILES            := lib/libvc1dec_sa.ca7.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libvcodec_oal
LOCAL_SRC_FILES            := lib/libvcodec_oal.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libvp8dec_sa.ca7
LOCAL_SRC_FILES            := lib/libvp8dec_sa.ca7.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libSwJpgCodec
LOCAL_SRC_FILES            := lib/libSwJpgCodec.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libmtkjpeg
LOCAL_SRC_FILES            := lib/libmtkjpeg.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)
