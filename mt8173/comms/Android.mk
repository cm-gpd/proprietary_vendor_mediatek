# Copyright (C) 2019 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

LOCAL_PATH := $(call my-dir)

ifeq ("$(wildcard hardware/mediatek/bluetooth/driver/mt66xx/Android.mk)","")
include $(CLEAR_VARS)
LOCAL_MODULE               := android.hardware.bluetooth@1.0-service-mediatek
LOCAL_SRC_FILES            := bin64/hw/android.hardware.bluetooth@1.0-service-mediatek
LOCAL_MODULE_CLASS         := EXECUTABLES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_INIT_RC              := etc/init/android.hardware.bluetooth@1.0-service-mediatek.rc
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := android.hardware.bluetooth@1.0-impl-mediatek
LOCAL_SRC_FILES            := lib64/hw/android.hardware.bluetooth@1.0-impl-mediatek.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := hw
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libbt-vendor
LOCAL_SRC_FILES            := lib64/libbt-vendor.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libbluetooth_mtk
LOCAL_SRC_FILES            := lib64/libbluetooth_mtk.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := android.hardware.bluetooth@1.0-impl-mediatek
LOCAL_SRC_FILES            := lib/hw/android.hardware.bluetooth@1.0-impl-mediatek.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := hw
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libbt-vendor
LOCAL_SRC_FILES            := lib/libbt-vendor.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libbluetooth_mtk
LOCAL_SRC_FILES            := lib/libbluetooth_mtk.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)
endif

ifeq ("$(wildcard hardware/mediatek/wlan/Android.mk)","")
include $(CLEAR_VARS)
LOCAL_MODULE               := android.hardware.wifi@1.0-service-mediatek
LOCAL_SRC_FILES            := bin64/hw/android.hardware.wifi@1.0-service-mediatek
LOCAL_MODULE_CLASS         := EXECUTABLES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_INIT_RC              := etc/init/android.hardware.wifi@1.0-service-mediatek.rc
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := lib_driver_cmd_mt66xx
LOCAL_SRC_FILES            := lib/lib_driver_cmd_mt66xx.a
LOCAL_MODULE_SUFFIX        := .a
LOCAL_MODULE_CLASS         := STATIC_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libwifi-hal-mt66xx
LOCAL_SRC_FILES            := lib/libwifi-hal-mt66xx.a
LOCAL_MODULE_SUFFIX        := .a
LOCAL_MODULE_CLASS         := STATIC_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := lib_driver_cmd_mt66xx
LOCAL_SRC_FILES            := lib64/lib_driver_cmd_mt66xx.a
LOCAL_MODULE_SUFFIX        := .a
LOCAL_MODULE_CLASS         := STATIC_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libwifi-hal-mt66xx
LOCAL_SRC_FILES            := lib64/libwifi-hal-mt66xx.a
LOCAL_MODULE_SUFFIX        := .a
LOCAL_MODULE_CLASS         := STATIC_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := p2p_supplicant_overlay.conf
LOCAL_SRC_FILES            := config/mtk-p2p_wpa_supplicant-overlay.conf
LOCAL_MODULE_CLASS         := ETC
LOCAL_MODULE_RELATIVE_PATH := wifi
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := wpa_supplicant_overlay.conf
LOCAL_SRC_FILES            := config/mtk-wpa_supplicant-overlay.conf
LOCAL_MODULE_CLASS         := ETC
LOCAL_MODULE_RELATIVE_PATH := wifi
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := wpa_supplicant.conf
LOCAL_SRC_FILES            := config/mtk-wpa_supplicant.conf
LOCAL_MODULE_CLASS         := ETC
LOCAL_MODULE_RELATIVE_PATH := wifi
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)
endif

ifeq ("$(wildcard hardware/mediatek/combo_tool/Android.mk)","")
include $(CLEAR_VARS)
LOCAL_MODULE               := wmt_launcher
LOCAL_SRC_FILES            := bin64/wmt_launcher
LOCAL_MODULE_CLASS         := EXECUTABLES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_COMMON_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := wmt_loader
LOCAL_SRC_FILES            := bin64/wmt_loader
LOCAL_MODULE_CLASS         := EXECUTABLES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_COMMON_PREBUILT)
endif

include $(CLEAR_VARS)
LOCAL_MODULE               := init_connectivity.rc
LOCAL_SRC_FILES            := etc/init/init_connectivity.rc
LOCAL_MODULE_CLASS         := ETC
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := init
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := init.bt_drv.rc
LOCAL_SRC_FILES            := etc/init/init.bt_drv.rc
LOCAL_MODULE_CLASS         := ETC
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := init
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := init.wlan_drv.rc
LOCAL_SRC_FILES            := etc/init/init.wlan_drv.rc
LOCAL_MODULE_CLASS         := ETC
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := init
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := init.wmt_drv.rc
LOCAL_SRC_FILES            := etc/init/init.wmt_drv.rc
LOCAL_MODULE_CLASS         := ETC
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := init
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := mt6630_patch_e3_0_hdr
LOCAL_SRC_FILES            := firmware/mt6630_patch_e3_0_hdr.bin
LOCAL_MODULE_SUFFIX        := .bin
LOCAL_MODULE_CLASS         := ETC
LOCAL_MODULE_PATH          := $(TARGET_OUT_VENDOR)/firmware
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := mt6630_patch_e3_1_hdr
LOCAL_SRC_FILES            := firmware/mt6630_patch_e3_1_hdr.bin
LOCAL_MODULE_SUFFIX        := .bin
LOCAL_MODULE_CLASS         := ETC
LOCAL_MODULE_PATH          := $(TARGET_OUT_VENDOR)/firmware
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := WIFI_RAM_CODE_MT6630
LOCAL_SRC_FILES            := firmware/WIFI_RAM_CODE_MT6630
LOCAL_MODULE_CLASS         := ETC
LOCAL_MODULE_PATH          := $(TARGET_OUT_VENDOR)/firmware
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := WMT
LOCAL_SRC_FILES            := firmware/WMT.cfg
LOCAL_MODULE_SUFFIX        := .cfg
LOCAL_MODULE_CLASS         := ETC
LOCAL_MODULE_PATH          := $(TARGET_OUT_VENDOR)/firmware
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := mt6630_ant_m1
LOCAL_SRC_FILES            := firmware/mt6630_ant_m1.cfg
LOCAL_MODULE_SUFFIX        := .cfg
LOCAL_MODULE_CLASS         := ETC
LOCAL_MODULE_PATH          := $(TARGET_OUT_VENDOR)/firmware
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
include $(BUILD_MTK_ARCH_PREBUILT)
