# Copyright (C) 2019 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Init scripts
PRODUCT_PACKAGES += \
    init_connectivity.rc \
    init.bt_drv.rc \
    init.wlan_drv.rc \
    init.wmt_drv.rc

# Firmware
PRODUCT_PACKAGES += \
    mt6630_ant_m1 \
    mt6630_patch_e3_0_hdr \
    mt6630_patch_e3_1_hdr \
    WIFI_RAM_CODE_MT6630 \
    WMT

PRODUCT_PROPERTY_OVERRIDES += \
    ro.vendor.wlan.gen=gen3
