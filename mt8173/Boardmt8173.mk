# Copyright (C) 2019 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

MT8173_PATH := vendor/mediatek/mt8173

include $(MT8173_PATH)/audio/BoardAudio.mk
#include $(MT8173_PATH)/keystore/BoardKeystore.mk
include $(MT8173_PATH)/media/BoardMedia.mk
include $(MT8173_PATH)/memtrack/BoardMemtrack.mk
include $(MT8173_PATH)/nvram/BoardNvram.mk
include $(MT8173_PATH)/powervr/BoardPowerVR.mk
include $(MT8173_PATH)/widevine/BoardWidevine.mk
