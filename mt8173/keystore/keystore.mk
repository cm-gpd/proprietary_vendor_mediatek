# Copyright (C) 2019 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

PRODUCT_PACKAGES += \
                    android.hardware.gatekeeper@1.0-impl \
                    android.hardware.gatekeeper@1.0-service \
                    android.hardware.keymaster@3.0-impl \
                    android.hardware.keymaster@3.0-service \
                    trustkernel.MT8173.rc \
                    trustkernel.rc \
                    teed \
                    gatekeeper.trustkernel \
                    keystore.v2.trustkernel

PRODUCT_PACKAGES += \
    02662e8e-e126-11e5-b86d9a79f06e9478.ta \
    0799a943-84a2-dead-0e3f8c88ad72507f.ta \
    2ea702fa-17bc-4752-b3adb2871a772347.ta \
    6f4d215d-5808-a000-ce96008c8378dd84.ta \
    9ef77781-7bd5-4e39-965f20f6f211f46b.ta \
    b46325e6-5c90-8252-2eada8e32e5180d6.ta \
    cfg.ini \
    6B6579626F785F6372797074

ifeq ($(TARGET_SUPPORTS_64_BIT_APPS),true)
PRODUCT_PACKAGES += \
                    android.hardware.gatekeeper@1.0-impl_32 \
                    android.hardware.keymaster@3.0-impl_32 \
                    gatekeeper.trustkernel_32 \
                    keystore.v2.trustkernel_32
endif

PRODUCT_PROPERTY_OVERRIDES += \
    ro.hardware.gatekeeper=trustkernel \
    ro.hardware.keystore=v2.trustkernel
