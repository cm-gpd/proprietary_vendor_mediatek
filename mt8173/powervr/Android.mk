# Copyright (C) 2019 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE               := rgx.fw.signed
LOCAL_SRC_FILES            := firmware/rgx.fw.signed
LOCAL_MODULE_CLASS         := ETC
LOCAL_MODULE_PATH          := $(TARGET_OUT_VENDOR)/firmware
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := ged_srv
LOCAL_SRC_FILES            := bin64/ged_srv
LOCAL_MODULE_CLASS         := EXECUTABLES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := ged_srv.rc
LOCAL_SRC_FILES            := etc/init/ged_srv.rc
LOCAL_MODULE_CLASS         := ETC
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_MODULE_RELATIVE_PATH := init
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := vendor.mediatek.hardware.pq@2.0-service
LOCAL_SRC_FILES            := bin64/hw/vendor.mediatek.hardware.pq@2.0-service
LOCAL_MODULE_CLASS         := EXECUTABLES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := hw
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := vendor.mediatek.hardware.pq@2.0-service.rc
LOCAL_SRC_FILES            := etc/init/vendor.mediatek.hardware.pq@2.0-service.rc
LOCAL_MODULE_CLASS         := ETC
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := init
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := vendor.mediatek.hardware.pq@2.0-impl
LOCAL_SRC_FILES            := lib64/hw/vendor.mediatek.hardware.pq@2.0-impl.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := hw
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := vendor.mediatek.hardware.pq@2.0_vendor
LOCAL_SRC_FILES            := lib64/vendor.mediatek.hardware.pq@2.0_vendor.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := gralloc.mt8173
LOCAL_SRC_FILES            := lib64/hw/gralloc.mt8173.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := hw
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := hwcomposer.mt8173
LOCAL_SRC_FILES            := lib64/hw/hwcomposer.mt8173.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := hw
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := vulkan.mt8173
LOCAL_SRC_FILES            := lib64/hw/vulkan.mt8173.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := hw
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libEGL_mtk
LOCAL_SRC_FILES            := lib64/egl/libEGL_mtk.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := egl
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libGLESv1_CM_mtk
LOCAL_SRC_FILES            := lib64/egl/libGLESv1_CM_mtk.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := egl
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libGLESv2_mtk
LOCAL_SRC_FILES            := lib64/egl/libGLESv2_mtk.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := egl
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libIMGegl
LOCAL_SRC_FILES            := lib64/libIMGegl.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libbwc
LOCAL_SRC_FILES            := lib64/libbwc.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libdpframework
LOCAL_SRC_FILES            := lib64/libdpframework.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libged
LOCAL_SRC_FILES            := lib64/libged.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libged_sys
LOCAL_SRC_FILES            := lib64/libged_sys.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libglslcompiler
LOCAL_SRC_FILES            := lib64/libglslcompiler.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libgpu_aux
LOCAL_SRC_FILES            := lib64/libgpu_aux.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libgralloc_extra
LOCAL_SRC_FILES            := lib64/libgralloc_extra.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libion_mtk
LOCAL_SRC_FILES            := lib64/libion_mtk.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libion_ulit
LOCAL_SRC_FILES            := lib64/libion_ulit.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libm4u
LOCAL_SRC_FILES            := lib64/libm4u.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libsrv_um
LOCAL_SRC_FILES            := lib64/libsrv_um.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libufwriter
LOCAL_SRC_FILES            := lib64/libufwriter.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libui_ext
LOCAL_SRC_FILES            := lib64/libui_ext.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libusc
LOCAL_SRC_FILES            := lib64/libusc.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libpq_cust
LOCAL_SRC_FILES            := lib64/libpq_cust.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libpq_prot
LOCAL_SRC_FILES            := lib64/libpq_prot.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := vendor.mediatek.hardware.pq@2.0-impl
LOCAL_SRC_FILES            := lib/hw/vendor.mediatek.hardware.pq@2.0-impl.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := hw
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := vendor.mediatek.hardware.pq@2.0_vendor
LOCAL_SRC_FILES            := lib/vendor.mediatek.hardware.pq@2.0_vendor.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := gralloc.mt8173
LOCAL_SRC_FILES            := lib/hw/gralloc.mt8173.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := hw
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := hwcomposer.mt8173
LOCAL_SRC_FILES            := lib/hw/hwcomposer.mt8173.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := hw
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := vulkan.mt8173
LOCAL_SRC_FILES            := lib/hw/vulkan.mt8173.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := hw
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libEGL_mtk
LOCAL_SRC_FILES            := lib/egl/libEGL_mtk.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := egl
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libGLESv1_CM_mtk
LOCAL_SRC_FILES            := lib/egl/libGLESv1_CM_mtk.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := egl
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libGLESv2_mtk
LOCAL_SRC_FILES            := lib/egl/libGLESv2_mtk.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := egl
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libIMGegl
LOCAL_SRC_FILES            := lib/libIMGegl.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libbwc
LOCAL_SRC_FILES            := lib/libbwc.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libdpframework
LOCAL_SRC_FILES            := lib/libdpframework.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libged
LOCAL_SRC_FILES            := lib/libged.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libged_sys
LOCAL_SRC_FILES            := lib/libged_sys.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libglslcompiler
LOCAL_SRC_FILES            := lib/libglslcompiler.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libgpu_aux
LOCAL_SRC_FILES            := lib/libgpu_aux.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libgralloc_extra
LOCAL_SRC_FILES            := lib/libgralloc_extra.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libion_ulit
LOCAL_SRC_FILES            := lib/libion_ulit.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libion_mtk
LOCAL_SRC_FILES            := lib/libion_mtk.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libm4u
LOCAL_SRC_FILES            := lib/libm4u.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libsrv_um
LOCAL_SRC_FILES            := lib/libsrv_um.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libufwriter
LOCAL_SRC_FILES            := lib/libufwriter.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libui_ext
LOCAL_SRC_FILES            := lib/libui_ext.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libusc
LOCAL_SRC_FILES            := lib/libusc.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libpq_cust
LOCAL_SRC_FILES            := lib/libpq_cust.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)
include $(CLEAR_VARS)
LOCAL_MODULE               := libpq_prot
LOCAL_SRC_FILES            := lib/libpq_prot.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)
