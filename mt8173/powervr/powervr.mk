# Copyright (C) 2019 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

PRODUCT_PACKAGES += \
                    android.hardware.graphics.allocator@2.0-impl \
                    android.hardware.graphics.allocator@2.0-service \
                    android.hardware.graphics.composer@2.1-impl \
                    android.hardware.graphics.composer@2.1-service \
                    android.hardware.graphics.mapper@2.0-impl \
                    vendor.mediatek.hardware.pq@2.0-service \
                    vendor.mediatek.hardware.pq@2.0-service.rc \
                    vendor.mediatek.hardware.pq@2.0-impl \
                    ged_srv \
                    ged_srv.rc \
                    gralloc.mt8173 \
                    hwcomposer.mt8173 \
                    vulkan.mt8173 \
                    libEGL_mtk \
                    libGLESv1_CM_mtk \
                    libGLESv2_mtk \
                    libpq_cust \
                    rgx.fw.signed

ifeq ($(TARGET_SUPPORTS_64_BIT_APPS),true)
PRODUCT_PACKAGES += \
                    android.hardware.graphics.allocator@2.0-impl_32 \
                    android.hardware.graphics.composer@2.1-impl_32 \
                    android.hardware.graphics.mapper@2.0-impl_32 \
                    gralloc.mt8173_32 \
                    vulkan.mt8173_32 \
                    libEGL_mtk_32 \
                    libGLESv1_CM_mtk_32 \
                    libGLESv2_mtk_32 \
                    libpq_cust_32
endif

PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.hardware.opengles.aep.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.opengles.aep.xml \
    frameworks/native/data/etc/android.hardware.vulkan.level-0.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.level.xml \
    frameworks/native/data/etc/android.hardware.vulkan.version-1_0_3.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.version.xml

PRODUCT_PROPERTY_OVERRIDES += \
    ro.opengles.version=196610
