# Copyright (C) 2019 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

LOCAL_PATH := $(call my-dir)

ifeq ("$(wildcard hardware/mediatek/audio/Android.mk)","")
include $(CLEAR_VARS)
LOCAL_MODULE               := audio.primary.mt8173
LOCAL_SRC_FILES            := lib64/hw/audio.primary.mt8173.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := hw
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libaedv
LOCAL_SRC_FILES            := lib64/libaedv.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libaudiocustparam_vendor
LOCAL_SRC_FILES            := lib64/libaudiocustparam_vendor.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libaudiotoolkit_vendor
LOCAL_SRC_FILES            := lib64/libaudiotoolkit_vendor.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libaudiocomponentengine_vendor
LOCAL_SRC_FILES            := lib64/libaudiocomponentengine_vendor.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libaudiodcrflt_vendor
LOCAL_SRC_FILES            := lib64/libaudiodcrflt_vendor.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libaudiosetting
LOCAL_SRC_FILES            := lib64/libaudiosetting.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libblisrc_vendor
LOCAL_SRC_FILES            := lib64/libblisrc_vendor.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libmrdumpv
LOCAL_SRC_FILES            := lib64/libmrdumpv.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libbessound_hd_mtk_vendor
LOCAL_SRC_FILES            := lib64/libbessound_hd_mtk_vendor.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libblisrc32_vendor
LOCAL_SRC_FILES            := lib64/libblisrc32_vendor.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libmtklimiter_vendor
LOCAL_SRC_FILES            := lib64/libmtklimiter_vendor.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libmtkshifter_vendor
LOCAL_SRC_FILES            := lib64/libmtkshifter_vendor.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libaudiocompensationfilter_vendor
LOCAL_SRC_FILES            := lib64/libaudiocompensationfilter_vendor.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := audio.primary.mt8173
LOCAL_SRC_FILES            := lib/hw/audio.primary.mt8173.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
LOCAL_MODULE_RELATIVE_PATH := hw
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libaedv
LOCAL_SRC_FILES            := lib/libaedv.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libaudiocustparam_vendor
LOCAL_SRC_FILES            := lib/libaudiocustparam_vendor.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libaudiotoolkit_vendor
LOCAL_SRC_FILES            := lib/libaudiotoolkit_vendor.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libaudiocomponentengine_vendor
LOCAL_SRC_FILES            := lib/libaudiocomponentengine_vendor.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libaudiodcrflt_vendor
LOCAL_SRC_FILES            := lib/libaudiodcrflt_vendor.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libaudiosetting
LOCAL_SRC_FILES            := lib/libaudiosetting.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libblisrc_vendor
LOCAL_SRC_FILES            := lib/libblisrc_vendor.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libmrdumpv
LOCAL_SRC_FILES            := lib/libmrdumpv.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libbessound_hd_mtk_vendor
LOCAL_SRC_FILES            := lib/libbessound_hd_mtk_vendor.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libblisrc32_vendor
LOCAL_SRC_FILES            := lib/libblisrc32_vendor.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libmtklimiter_vendor
LOCAL_SRC_FILES            := lib/libmtklimiter_vendor.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libmtkshifter_vendor
LOCAL_SRC_FILES            := lib/libmtkshifter_vendor.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libaudiocompensationfilter_vendor
LOCAL_SRC_FILES            := lib/libaudiocompensationfilter_vendor.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)
endif

include $(CLEAR_VARS)
LOCAL_MODULE               := libmtk_drvb
LOCAL_SRC_FILES            := lib64/libmtk_drvb.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm64
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := libmtk_drvb
LOCAL_SRC_FILES            := lib/libmtk_drvb.so
LOCAL_MODULE_SUFFIX        := .so
LOCAL_MODULE_CLASS         := SHARED_LIBRARIES
LOCAL_MODULE_TARGET_ARCH   := arm
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := mediatek
LOCAL_VENDOR_MODULE        := true
include $(BUILD_MTK_ARCH_PREBUILT)
